import React, {
  createContext, useEffect, useState,
} from 'react';
import PropTypes from 'prop-types';

export const Global = createContext({});

export const GlobalProvider = ({ children }) => {
  const [count, setCount] = useState(0);
  useEffect(
    () => {
      const timeoutId = setTimeout(
        () => {
          setCount(count + 1);
        },
        1000,

      );
      return () => clearTimeout(timeoutId);
    },
    [count],
  );
  return (
    <Global.Provider value={{ count }}>
      {children}
    </Global.Provider>
  );
};

GlobalProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
