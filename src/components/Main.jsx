import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Global } from 'contexts/Global';
import logo from '../logo.svg';

export const MainComponent = () => {
  const { t } = useTranslation();
  const { count } = useContext(Global);
  return (
    <div className="App">
      <img data-testid="react-logo" src={logo} className="App-logo" alt="logo" />
      <p data-testid="lorem-texts">
        {t('sample.loremIpsum')}
      </p>
      <span>{`${t('sample.counter')}${count}`}</span>
    </div>
  );
};

export default MainComponent;
