import '@testing-library/jest-dom/extend-expect';
import i18next from 'i18next';
import { toHaveNoViolations } from 'jest-axe';
import './i18n';

i18next.changeLanguage('cimode');
expect.extend(toHaveNoViolations);
