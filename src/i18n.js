import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import translation from 'locales/en-US.json';

// the translations
const resources = {
  en: {
    translation,
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'en-US', // Make this dynamic
    /*
    // keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
      format(value, format, lng) {
        if (format === 'uppercase') return value.toUpperCase();
        if (value instanceof Date) return moment(value).format(format);
        return value;
      },
    },
     */
  });

export default i18n;
