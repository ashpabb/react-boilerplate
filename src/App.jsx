import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import { lightTheme } from 'material';
import { ToastContainer } from 'react-toastify';
import Main from 'components/Main';
import { GlobalProvider } from 'contexts/Global';

function App() {
  return (
    <GlobalProvider>
      <ThemeProvider theme={lightTheme}>
        <CssBaseline />
        <Router>
          <Switch>
            <Route component={Main} />
          </Switch>
        </Router>
      </ThemeProvider>
      <ToastContainer position="bottom-right" newestOnTop />
    </GlobalProvider>
  );
}

export default App;
