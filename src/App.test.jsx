import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';
import { axe } from 'jest-axe';
import App from './App';

describe('App Component', () => {
  it('will pass a11y tests', async () => {
    const { container } = render(
      <MemoryRouter initialEntries={['/info']}>
        <App />
      </MemoryRouter>,
    );

    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
