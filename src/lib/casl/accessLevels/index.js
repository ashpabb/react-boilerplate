export const accessLevels = {
  noAccess: 'noAccess',
  readOnly: 'readOnly',
  example: 'example',
  admin: 'admin',
};
