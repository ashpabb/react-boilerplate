import { Ability, detectSubjectType } from '@casl/ability';
import { accessLevels } from 'lib/casl/accessLevels';
import exampleAbility from './example';

/**
 * A custom function to determine CASL subject type
 * This let's us use regular objects instead of defaulting
 * To require Class-based permissions
 * @param {any} subject
 */
function detectAppSubjectType(subject) {
  if (subject && typeof subject === 'object' && subject.type) {
    return subject.type;
  }

  return detectSubjectType(subject);
}

/**
 * Output a CASL ability based on user.accessLevel
 * @param {any} user
 */
export const makeAbility = (user) => new Ability(
  (() => {
    switch (user.accessLevel) {
      case accessLevels.example:
      default:
        return exampleAbility(user);
    }
  })(),
  {
    detectSubjectType: detectAppSubjectType,
  },
);
