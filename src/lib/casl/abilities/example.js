import { AbilityBuilder } from '@casl/ability';

/**
 * CASL-based Abilities for a Viewer (ReadOnly) Role
 * @param { object } user contains details about logged in user
 * @return { RawRuleOf } rules object
 */
function defineViewerAbilities() {
  const { can, rules } = new AbilityBuilder();

  // Can view all resources
  can('view', 'all');

  return rules;
}

export default defineViewerAbilities;
