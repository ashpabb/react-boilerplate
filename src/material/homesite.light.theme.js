import { createMuiTheme } from '@material-ui/core/styles';

const lightTheme = createMuiTheme({
  palette: {
    primary: { main: '#094B7F' },
    secondary: { main: '#00802C' },
    background: { default: 'F4F9FD' },
  },
});

export { lightTheme };
