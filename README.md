# React Redux Boiler plate

React boilerplate using Material UI and React-Router.
This project was bootstrapped with [Create React App][1].

## Localization
This boilerplate recommends [i18Next][p5] to provide localization.
The initialization for React-i18Next occurs by loading `src/i18n.js`.
See `src/components/Main.jsx` for an example of using translation hook.

## Code Quality
Code quality must be treated as a first-class citizen of VC output.
ESLint rules are pre-configured from [airbnb standards][p6] and enforced
via [Husky][p7] commit and push hooks.

React code will be built solely using functional components and hooks
as a hard preference.

## Testing
Jest testing is first and foremost to give baseline assurance for accessibility
but also to enable User Story Driven testing. Implementation in development...

## IDE Configuration

### VS Code
The following plugins are recommended:
- [ESLint][p4]: ECMAScript Linter
- [markdownlint][p1]: For linting MD files
- [Jest][p2]: Testing platform and code coverage highlighter
- [GitLens][p3]: Visualize code authorship at a glance via Git blame annotations

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Is hosted on [localhost:3000][2].

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests][3] for more information.

Run `npm test -- --coverage` to get a code coverage report.

For VS Code, Jest has a plugin which can show code coverage: [read here][4]

### `npm build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the
best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the CRA docs section about [deployment][5] for more information.

[1]: https://github.com/facebook/create-react-app
[2]: http://localhost:3000
[3]: https://facebook.github.io/create-react-app/docs/running-tests
[4]: https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest#how-do-i-show-code-coverage
[5]: https://facebook.github.io/create-react-app/docs/deployment
[p1]: https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint
[p2]: https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest
[p3]: https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens
[p4]: https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
[p5]: https://www.i18next.com/
[p6]: https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb
[p7]: https://typicode.github.io/husky/
